
export default {
    title: "宋长昊的猫",
    description: '宋长昊的猫.',
    themeConfig: {
        siteTitle: '宋长昊的猫',
        nav: [
            { text: '0915', link: '/cats/20220915/' },
            { text: '0926', link: '/cats/20220926/' },
            { text: '1001', link: '/cats/20221001/' },
            { text: '1002', link: '/cats/20221002/' },
            { text: '1003', link: '/cats/20221003/' },
            { text: '1004', link: '/cats/20221004/' },
            { text: '1005', link: '/cats/20221005/' },
        ]
    },
    head: [
        ['script', {
            src: "https://umami.geyinchi.cn/umami.js",
            'data-website-id': "27f7973b-c4d4-4f63-ab39-a010423c59eb",
            async: '',
            defer: ''
        }]
    ]
}
